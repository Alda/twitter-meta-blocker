extern crate dotenv;
extern crate oauthcli;
extern crate url;

use dotenv::dotenv;
use std::env;
use oauthcli::*;
use url::Url;

fn main() {
    dotenv().ok();

    let url = &Url::parse("https://api.twitter.com/").unwrap();

    let consumer_token = env::var("CONSUMER_TOKEN").unwrap();
    let consumer_secret = env::var("CONSUMER_SECRET").unwrap();
    let client_token = env::var("CLIENT_TOKEN").unwrap();
    let client_secret = env::var("CLIENT_SECRET").unwrap();

    let token_get = OAuthAuthorizationHeaderBuilder::new(
        "GET",
        url,
        &consumer_token,
        &consumer_secret,
        SignatureMethod::HmacSha1
    )
    .token(&client_token, &client_secret)
    .finish();

    let token_post = OAuthAuthorizationHeaderBuilder::new(
        "POST", url, consumer_token, consumer_secret, SignatureMethod::HmacSha1)
        .token(client_token, client_secret)
        .finish();

    println!("{} {}", token_get.to_string(), token_post.to_string());
}
